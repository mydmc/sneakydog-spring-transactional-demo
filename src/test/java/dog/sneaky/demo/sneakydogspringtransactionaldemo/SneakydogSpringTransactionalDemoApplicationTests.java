package dog.sneaky.demo.sneakydogspringtransactionaldemo;

import dog.sneaky.demo.sneakydogspringtransactionaldemo.test.BaseTestService;
import dog.sneaky.demo.sneakydogspringtransactionaldemo.test.GGGGtest;
import dog.sneaky.demo.sneakydogspringtransactionaldemo.test.TestContext;
import dog.sneaky.demo.sneakydogspringtransactionaldemo.test.UUUUserver;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

@SpringBootTest
class SneakydogSpringTransactionalDemoApplicationTests {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private GGGGtest gggGtest;

    @Autowired
    private UUUUserver uuuUserver;

    @Test
    void contextLoads() throws Exception {
        BaseTestService baseTestService = applicationContext.getBean("test2", BaseTestService.class);
        baseTestService.execute(new TestContext());
//        gggGtest.test();
    }



    @Test
    void contextLoads2() throws Exception {
        gggGtest.test();
    }



    @Test
    void uuuUserverget() throws Exception {
        uuuUserver.get();
    }






}
