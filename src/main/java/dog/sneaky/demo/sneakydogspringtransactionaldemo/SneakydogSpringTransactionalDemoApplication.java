package dog.sneaky.demo.sneakydogspringtransactionaldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.annotation.Order;
//
@SpringBootApplication
@ImportResource("classpath:transaction.xml")
public class SneakydogSpringTransactionalDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SneakydogSpringTransactionalDemoApplication.class, args);
    }

}
