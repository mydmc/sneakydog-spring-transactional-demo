package dog.sneaky.demo.sneakydogspringtransactionaldemo.test;


import lombok.Data;

@Data
public class TestContext {

    private String name;
}
