package dog.sneaky.demo.sneakydogspringtransactionaldemo.test;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Consumer;
import java.util.function.Supplier;

@Component
class TransactionHandler {
    @Transactional
    public <T> void runTransaction(Supplier<T> supplier) {
        supplier.get();
    }


}
