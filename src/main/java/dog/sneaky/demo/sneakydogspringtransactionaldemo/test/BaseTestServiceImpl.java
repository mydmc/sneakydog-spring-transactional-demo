package dog.sneaky.demo.sneakydogspringtransactionaldemo.test;


import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;

@Slf4j
public abstract class BaseTestServiceImpl implements BaseTestService {
    @Resource
    protected TransactionHandler transactionHandler;
    @Resource
    protected JdbcTemplate jdbcTemplate;

    // @Transactional直接Call的方法才有效
    abstract protected void process(TestContext context) throws RuntimeException;

    @Override
    public void execute(TestContext context) throws Exception {
        try {
            transactionHandler.runTransaction(() -> {
                process(context);
                return true;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
