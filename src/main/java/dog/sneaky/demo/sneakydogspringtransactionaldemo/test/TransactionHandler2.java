package dog.sneaky.demo.sneakydogspringtransactionaldemo.test;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
class TransactionHandler2{

    @Transactional
    public void run(BaseTestServiceImpl baseTestService, TestContext context) {
        baseTestService.process(context);
    }
}
