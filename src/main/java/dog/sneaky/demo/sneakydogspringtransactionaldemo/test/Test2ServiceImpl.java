package dog.sneaky.demo.sneakydogspringtransactionaldemo.test;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;

@Slf4j
@Component("test2")
@RequiredArgsConstructor
public class Test2ServiceImpl extends BaseTestServiceImpl {

    private final TransactionTemplate transactionTemplate;

    @Override
    protected void process(TestContext context) throws RuntimeException {
        System.out.println("Test2ServiceImpl -- Transactional--start");
        jdbcTemplate.update("insert into test_tx(name) values('123131313')");
        throw new RuntimeException("12313");
    }




}
