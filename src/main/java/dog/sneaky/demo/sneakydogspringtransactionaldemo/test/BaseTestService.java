package dog.sneaky.demo.sneakydogspringtransactionaldemo.test;


public interface BaseTestService {

    void execute(TestContext context) throws Exception;
}
