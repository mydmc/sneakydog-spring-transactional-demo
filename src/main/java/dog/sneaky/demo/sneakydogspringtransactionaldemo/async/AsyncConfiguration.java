package dog.sneaky.demo.sneakydogspringtransactionaldemo.async;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;



@Configuration
@EnableAsync
public class AsyncConfiguration {

    @Bean("taskExecutor")
    public TaskExecutor taskExecutor(){
        SimpleAsyncTaskExecutor simpleAsyncTaskExecutor = new SimpleAsyncTaskExecutor();
        simpleAsyncTaskExecutor.setConcurrencyLimit(1);
        return simpleAsyncTaskExecutor;
    }



}
