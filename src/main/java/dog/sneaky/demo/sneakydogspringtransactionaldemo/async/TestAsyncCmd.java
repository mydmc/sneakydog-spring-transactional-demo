package dog.sneaky.demo.sneakydogspringtransactionaldemo.async;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class TestAsyncCmd implements ApplicationRunner {


    private final TestAsync testAsync;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        testAsync.teset1();
        testAsync.teset1();
    }
}
